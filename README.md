#### 介绍
如果您不关注git版本控制信息，或者不习惯使用git工具，那么您可以通过这个仓库下载到SDK的压缩包形态。Linux SDK和Baremetal SDK各自独立管理，您可以通过Changelog文件看到二者各版本的主要更新记录。本仓库只维护最新的几个版本，过老的版本将会被删除。

#### 下载
在浏览器页面上鼠标点击下载即可。当然，git也是肯定支持的。

#### Linux SDK解压缩
> 1. 使用命令`cat TIH64V690_SDK_Linux_V0.9.3.tar.gz.* > TIH64V690_SDK_Linux_V0.9.3.tar.gz`合并分隔的压缩包。
> 2. 使用命令`tar xvf TIH64V690_SDK_Linux_V0.9.3.tar.gz`完成解压缩

#### 使用说明
压缩包解压之后，其Doc 目录下的《TIH64Vx690 SDK Quick Start》文档是 SDK 的入门指引，建议优先阅读此文档。
